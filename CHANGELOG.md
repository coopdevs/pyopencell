# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## Removed

* Remove support for Python 2.7 [#51](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/51)

## [v0.4.5]

## Add

* Add the Operation Date and Description options for the One Shot charge [#48](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/48)

## [v0.4.4]

## Add

* Add update method to Customer [#47](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/47)


## [v0.4.3]

## Add

* Add the amount without taxes option for the One Shot charge [#45](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/45)

## [v0.4.2]

## Changed

* Fix publish CI job [#44](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/44)

## [v0.4.1]

## Add

* Add the CustomerAccount resource [#41](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/41)

## [v0.4.0]

## Add

* Add subscription list endpoint [#40](https://gitlab.com/coopdevs/pyopencell/-/merge_requests/40)

## [v0.3.2]

* Raise error if status fail [#39](https://gitlab.com/coopdevs/pyopencell/merge_requests/39)

## [v0.3.1]

## Add

* Upload package to PyPI with Gitlab-ci [#37](https://gitlab.com/coopdevs/pyopencell/merge_requests/37)
* Add to PyOpenCellAPIException an attribute with the API body error [#38](https://gitlab.com/coopdevs/pyopencell/merge_requests/38)
* Add method to send an invoice by email [#36](https://gitlab.com/coopdevs/pyopencell/merge_requests/36)
